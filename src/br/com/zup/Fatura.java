package br.com.zup;

import java.sql.Struct;

public class Fatura {
    private Consumidor consumidor;
    private double valor;
    private String dataDeValidade;

    Fatura(){}

    public Fatura(Consumidor consumidor, double valor, String dataDeValidade) {
        this.consumidor = consumidor;
        this.valor = valor;
        this.dataDeValidade = dataDeValidade;
    }

    public Consumidor getConsumidor() {
        return consumidor;
    }

    public void setConsumidor(Consumidor consumidor) {
        this.consumidor = consumidor;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDataDeValidade() {
        return dataDeValidade;
    }

    public void setDataDeValidade(String dataDeValidade) {
        this.dataDeValidade = dataDeValidade;
    }
    // Curiosidade mostrando os dados de forma parcial
    public String mostrarValorEData(){
        StringBuilder retorno = new StringBuilder();
        retorno.append("O valor da fatura é: R$"+valor);
        retorno.append("A data de validade é: "+dataDeValidade);
        return retorno.toString();
    }

    @Override
    public String toString(){
        StringBuilder retorno = new StringBuilder();
        retorno.append("\n -------------------- \n");
        retorno.append("Consumidor: "+consumidor+ "\n");
        retorno.append("Valor da fatura: " +valor+ "\n");
        retorno.append("Data de validade: "+dataDeValidade + "\n");
        retorno.append("-------------------- \n");
        return retorno.toString();
    }
}
