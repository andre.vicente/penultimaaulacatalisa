package br.com.zup;

import java.util.ArrayList;
import java.util.List;

public class ServicoFatura {
    private static List <Fatura> faturas= new ArrayList<>();

    public static Fatura cadastrarFatura(String email, double valor, String dataDaValidade) throws Exception{
        Consumidor consumidor = ServicoConsumidor.pesquisarConsumidorPorEmail(email);
        Fatura fatura = new Fatura(consumidor, valor, dataDaValidade);
        faturas.add(fatura);
        return fatura;


    }


}
