package br.com.zup;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ServicoConsumidor {
    private static List <Consumidor> consumidores = new ArrayList<>();

    public static void validarEmail(String email) throws Exception{
        if (!email.contains("@")) {
            throw new Exception("Email inválido");
        }
    }

    public static void verificarSeEmailExiste(String email) throws Exception{
        for (Consumidor consumidor : consumidores){
            if(consumidor.getEmail().equals(email)){
                throw new Exception("O email já existe");
            }
        }
    }
    public static Consumidor cadastrarConsumidor(String nome, String email) throws Exception {
        validarEmail(email);
        verificarSeEmailExiste(email);
        Consumidor obj_consumidor = new Consumidor(nome, email);
        consumidores.add(obj_consumidor);
        return obj_consumidor;
    }
    public static Consumidor pesquisarConsumidorPorEmail(String email) throws Exception{
        validarEmail(email);
        for (Consumidor consumidor : consumidores){
            if (consumidor.getEmail().equals(email)){
                return consumidor;
            }
        }
        throw new Exception("Consumidor não cadastrado");
    }
}
